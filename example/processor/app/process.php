<?php

use Bunny\Channel;
use Bunny\Client;
use Bunny\Message;

require_once 'vendor/autoload.php';

$sqlDb = $_ENV['MARIADB_NAME'] ?? 'megado';
$sqlHost = $_ENV['MARIADB_HOST'] ?? 'localhost';
$pdo = new PDO("mysql:dbname={$sqlDb};host={$sqlHost}", $_ENV['MARIADB_USER'] ?? 'root', $_ENV['MARIADB_PASSWORD'] ?? 'root');

$predis = new \Predis\Client([
    'scheme' => 'tcp',
    'host'   => $_ENV['REDIS_HOST'] ?? 'localhost',
    'port'   => $_ENV['REDIS_PORT'] ?? 6379,
]);

$client = new Client([
    'heartbeat' => 60.0,
    'host'      => $_ENV['RABBIT_HOST'] ?? 'localhost',
    'vhost'     => $_ENV['RABBIT_VHOST'] ?? 'megado',
    'user'      => $_ENV['RABBIT_USER'] ?? 'admin',
    'password'  => $_ENV['RABBIT_PASSWORD'] ?? 'admin',
]);
$client->connect();
$channel = $client->channel();

$channel->exchangeDeclare('process', 'direct',false, true, false);
$channel->queueDeclare('process', false, true,false, false);
$channel->queueBind('process', 'process', 'process');

$channel->exchangeDeclare('export', 'direct',false, true, false);
$channel->queueDeclare('export', false, true,false, false);
$channel->queueBind('export', 'export', 'export');

$appHostname = $_ENV['APP_HOST'] ?? 'localhost';

$channel->run(function(Message $message, Channel $channel, Client $client) use ($predis, $pdo, $appHostname) {
    $messageData = json_decode($message->content);

    $redisData = $predis->get($messageData->item_id);
    $appRequest = json_encode([
        'data' => [
            [
                'ITEM_ID' => $messageData->item_id,
                'PRODUCT' => $redisData,
            ]
        ]
    ]);

    $ch = curl_init("http://{$appHostname}/");
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_POSTFIELDS, $appRequest);
    $response = json_decode(curl_exec($ch));

    foreach($response->data as $item) {
        $sth = $pdo->prepare('INSERT INTO items (item_id, product) VALUES (?, ?) ON DUPLICATE KEY UPDATE product=VALUES(product)');
        $sth->execute([$item->ITEM_ID, $item->PRODUCT]);
    }

    if($messageData->last) {
        $channel->publish(json_encode([
            'file' => 'output-feed.xml',
        ]),['delivery-mode' => 2], 'export', 'export');
    }

    $channel->ack($message);
    echo date('Y-m-d H:i:s')." Item processed\n";
},'process');
$channel->close();
