<?php
if($_SERVER['REQUEST_METHOD'] !== 'POST') {
    http_response_code(405);
    die('HTTP 405 - Method not allowed');
}

$request = json_decode(file_get_contents('php://input'));
foreach($request->data as $key => $value) {
    $request->data[$key]->PRODUCT = $value->PRODUCT.' - processed '.date('Y-m-d H:i:s');
}
echo json_encode($request);
