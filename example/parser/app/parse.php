<?php

use Bunny\Channel;
use Bunny\Client;
use Bunny\Message;

require_once 'vendor/autoload.php';

$predis = new \Predis\Client([
    'scheme' => 'tcp',
    'host'   => $_ENV['REDIS_HOST'] ?? 'localhost',
    'port'   => $_ENV['REDIS_PORT'] ?? 6379,
]);

$client = new Client([
    'heartbeat' => 60.0,
    'host'      => $_ENV['RABBIT_HOST'] ?? 'localhost',
    'vhost'     => $_ENV['RABBIT_VHOST'] ?? 'megado',
    'user'      => $_ENV['RABBIT_USER'] ?? 'admin',
    'password'  => $_ENV['RABBIT_PASSWORD'] ?? 'admin',
]);
$client->connect();
$channel = $client->channel();

$channel->exchangeDeclare('parse', 'direct',false, true, false);
$channel->queueDeclare('parse', false, true,false, false);
$channel->queueBind('parse', 'parse', 'parse');

$channel->exchangeDeclare('process', 'direct',false, true, false);
$channel->queueDeclare('process', false, true,false, false);
$channel->queueBind('process', 'process', 'process');

$channel->run(function(Message $message, Channel $channel, Client $client) use ($predis) {
    $messageData = json_decode($message->content);
    if(!file_exists($messageData->file)) {
        echo "Message rejected.\n";
        $channel->reject($message, false);
        return;
    }

    $parsed = simplexml_load_file($messageData->file);
    $items = $parsed->count();
    $cnt = 0;
    foreach($parsed as $shopitem) {
        $predis->set((string)$shopitem->ITEM_ID, (string)$shopitem->PRODUCT);

        $channel->publish(json_encode([
            'item_id' => (string)$shopitem->ITEM_ID,
            'last' => (++$cnt) >= $items,
        ]),['delivery-mode' => 2], 'process', 'process');
    }

    $channel->ack($message);
    echo date('Y-m-d H:i:s')." Feed parsed\n";
},'parse');
$channel->close();
