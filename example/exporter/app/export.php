<?php

use Bunny\Channel;
use Bunny\Client;
use Bunny\Message;

require_once 'vendor/autoload.php';

$sqlDb = $_ENV['MARIADB_NAME'] ?? 'megado';
$sqlHost = $_ENV['MARIADB_HOST'] ?? 'localhost';
$pdo = new PDO("mysql:dbname={$sqlDb};host={$sqlHost}", $_ENV['MARIADB_USER'] ?? 'root', $_ENV['MARIADB_PASSWORD'] ?? 'root');

$client = new Client([
    'heartbeat' => 60.0,
    'host'      => $_ENV['RABBIT_HOST'] ?? 'localhost',
    'vhost'     => $_ENV['RABBIT_VHOST'] ?? 'megado',
    'user'      => $_ENV['RABBIT_USER'] ?? 'admin',
    'password'  => $_ENV['RABBIT_PASSWORD'] ?? 'admin',
]);
$client->connect();
$channel = $client->channel();

$channel->exchangeDeclare('export', 'direct',false, true, false);
$channel->queueDeclare('export', false, true,false, false);
$channel->queueBind('export', 'export', 'export');

$channel->run(function(Message $message, Channel $channel, Client $client) use ($pdo) {
    $messageData = json_decode($message->content);

    $output = '<?xml version="1.0" encoding="utf-8"?>';
    $output .= "\n<SHOP>\n";
    $st = $pdo->prepare('SELECT * FROM items');
    $st->execute();
    $sqlData = $st->fetchAll();
    foreach($sqlData as $row) {
        $output .= "\t<SHOPITEM>\n";
        $output .= "\t\t<ITEM_ID>{$row['item_id']}</ITEM_ID>\n";
        $output .= "\t\t<PRODUCT>{$row['product']}</PRODUCT>\n";
        $output .= "\t</SHOPITEM>\n";
    }
    $output .= "</SHOP>\n";
    $filename = "/app/data/{$messageData->file}";
    file_put_contents($filename, $output);
    $channel->ack($message);
    echo date('Y-m-d H:i:s')." Feed exported to {$filename}\n";
},'export');
$channel->close();
