<?php

$sqlDb = $_ENV['MARIADB_NAME'] ?? 'megado';
$sqlHost = $_ENV['MARIADB_HOST'] ?? 'localhost';
$pdo = new PDO("mysql:dbname={$sqlDb};host={$sqlHost}", $_ENV['MARIADB_USER'] ?? 'root', $_ENV['MARIADB_PASSWORD'] ?? 'root');

echo '<h1>MEGADO</h1>';
echo '<table>';
echo '<tr><th>ITEM_ID</th><th>PRODUCT</th></tr>';
$st = $pdo->prepare('SELECT * FROM items');
$st->execute();
$sqlData = $st->fetchAll();
foreach($sqlData as $row) {
    echo "<tr><td>{$row['item_id']}</td><td>{$row['product']}</td></tr>";
}
echo '</table>';
echo '<style>td,th {border: 1px solid black; padding: 5px;} table {border-collapse: collapse;}</style>';
