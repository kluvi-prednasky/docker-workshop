<?php
use Bunny\Client;

require_once 'vendor/autoload.php';
$feed = 'https://bdfx.cz/docker/feed.xml';
$target = '/app/data/test.xml';

copy($feed, $target);

$client = new Client([
    'heartbeat' => 60.0,
    'host'      => $_ENV['RABBIT_HOST'] ?? 'localhost',
    'vhost'     => $_ENV['RABBIT_VHOST'] ?? 'megado',
    'user'      => $_ENV['RABBIT_USER'] ?? 'admin',
    'password'  => $_ENV['RABBIT_PASSWORD'] ?? 'admin',
]);
$client->connect();
$channel = $client->channel();

$channel->exchangeDeclare('parse', 'direct',false, true, false);
$channel->queueDeclare('parse', false, true,false, false);
$channel->queueBind('parse', 'parse', 'parse');

$channel->publish(json_encode([
    'url' => $feed,
    'file' => $target,
]),['delivery-mode' => 2], 'parse', 'parse');
$channel->close();
echo date('Y-m-d H:i:s')." {$feed} downloaded to {$target} and published to RabbitMQ\n";
