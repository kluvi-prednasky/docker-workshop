# Docker (&microservices) workshop
## Prezentace
https://docs.google.com/presentation/d/158o39Fe7rUuWnwiRmRWtIAERFFZZk1ue5NgkIlME86Y/edit?usp=sharing

## Zadání
Vytvořit aplikaci MEGADO (MEGA DOcker) která bude sloužit pro zpracování XML feedů.
- každou hodinu stáhne feed
- rozparsuje ho
- uloží do Redisu
- "aplikuje pravidla" po jednotlivých produktech (zavolá jinou aplikaci přes REST API)
- výsledek uloží do MariaDB
- vygeneruje výstupní feed, který bude na nějaké URL
- bude mít jednoduchý frontend co jen vypíše výstupní data
- Adminer

### Společné kontejnery (na data):
- RabbitMQ na fronty [5min]
  - https://www.rabbitmq.com/getstarted.html
  - https://github.com/jakubkulhan/bunny
- Redis na mezivýsledky [5min]
- MariaDB na ukládání dat [5min]

### Aplikační kontejnery:
1. kontejner (downloader) [15min]
    - každou hodinu stáhne vstupní feed na disk
1. kontejner (parser) [15min]
    - po stažení feedu data rozparsuje a uloží do Redisu
1. kontejner (processor) [15min]
    - "aplikuje pravidla" - pošle data do jiného kontejneru (aplikace) a výsledek uloží do SQL
1. kontejner (app) [15min]
    - "aplikace" (připojí k názvu produktu aktuální čas)
1. kontejner (exporter) [15min]
    - vygeneruje výstupní feed na disk
    - naivní přístup - po zpracování posledního produktu
1. kontejner (output) [5min]
    - HTTP server pro výstupní feedy
1. kontejner (frontend) [15min]
    - frontend - vypíše data z SQL
1. kontejner (adminer) [5min]

### Poznámky
- odhad času: cca 2h
- https://bdfx.cz/docker/feed.xml (předpokládáme, že feed má unikátní ITEM_ID a jediný další element je PRODUCT)
- nebudeme dělat nějaký over-engineering (KISS)
- URL adresa vstupního feedu,... může být zapsaná natvrdo v kódu
- co nebude natvrdo v kódu jsou konfigurace potřebné pro komunikaci vzájemně mezi kontejnery,...
- není nezbytně nutné použít zrovna RabbitMQ, ale vzhledem k jednoduchosti je to doporučená varianta
- fronty pro RabbitMQ není třeba definovat ve scriptu, stačí naklikat v rozhraní
- RabbitMQ - do docker-compose je potřeba přidat "hostname: service-rabbitmq", jinak se budou "hromadit instance" při restartu kontejneru
